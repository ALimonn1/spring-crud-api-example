package com.galvanize.UnitTests;

import com.galvanize.controller.UsersController;
import com.galvanize.model.AuthResponse;
import com.galvanize.model.User;
import com.galvanize.service.UsersService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UsersController.class)
public class UsersControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsersService usersService;

    @Test
    void usersEndpointShouldReturnListOfUsers() throws Exception {
        List<User> users = new ArrayList<>();
        users.add(new User(1, "john@example.com"));
        users.add(new User(2, "alicia@example.com"));


        when(usersService.getUsers())
                .thenReturn(users);

        mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("users.[0].id").value(1))
                .andExpect(jsonPath("users.[0].email").value("john@example.com"))
                .andExpect(jsonPath("users.[1].id").value(2))
                .andExpect(jsonPath("users.[1].email").value("alicia@example.com"));

    }

    @Test
    void usersEndpointShouldInsertUsers() throws Exception {
        String request = "" +
                "{\n" +
                "    \"email\": \"alex@example.com\",\n" +
                "    \"password\": \"something-secret\"\n" +
                "}";

        when(usersService.addUser(any()))
                .thenReturn(new User(5, "alex@example.com"));

        mockMvc.perform(
                post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk())
                .andExpect(jsonPath("email").value("alex@example.com"))
                .andExpect(jsonPath("id").value(5));
    }

    @Test
    void usersEndpointShouldGetUserById() throws Exception {
        when(usersService.getUser(anyLong()))
                .thenReturn(new User(1, "john@example.com"));

        mockMvc.perform(get("/users/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("user.id").value(1))
                .andExpect(jsonPath("user.email").value("john@example.com"));

    }

    @Test
    void usersEndpointShouldUpdateUserById() throws Exception {
        String request = "" +
                "{\n" +
                "    \"email\": \"john@example.com\",\n" +
                "    \"password\": \"something-secret\"\n" +
                "}";

        when(usersService.updateUser(any()))
                .thenReturn(new User(1, "john@example.com"));

        mockMvc.perform(
                patch("/users/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(1))
                .andExpect(jsonPath("email").value("john@example.com"));
    }

    @Test
    void usersEndpointShouldDeleteUserById() throws Exception{

        Map<String, Long> mockedDeleteReturn = new HashMap<String, Long>() {
            {
                put("count", (long) 32);
            }
        };

        when(usersService.deleteUser(anyLong()))
                .thenReturn(mockedDeleteReturn);

        mockMvc.perform(delete("/users/5"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("count").value(32));

    }

    @Test
    void usersEndpointShouldAuthenticateUser() throws Exception{
        String request = "" +
                "{\n" +
                "    \"email\": \"angelica@example.com\",\n" +
                "    \"password\": \"1234\"\n" +
                "}";

        AuthResponse authResponse = new AuthResponse(new User(6, "angelica@example.com"), true);

        when(usersService.authenticateUser(any()))
                .thenReturn(authResponse);

        mockMvc.perform(
                post("/users/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("user.email").value("angelica@example.com"))
                .andExpect(jsonPath("user.id").value(6))
                .andExpect(jsonPath("authenticated").value(true));
    }

}
