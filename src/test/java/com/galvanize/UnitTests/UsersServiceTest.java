package com.galvanize.UnitTests;

import com.galvanize.model.User;
import com.galvanize.repository.UsersRepository;
import com.galvanize.service.UsersService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UsersServiceTest {

    @Mock
    private UsersRepository usersRepository;

    private UsersService usersService;

    @BeforeEach
    void setup(){
        usersService = new UsersService(usersRepository);
    }

    @Test
    void userServiceShouldReturnUsersList() {
        List<User> users = new ArrayList<>();
        users.add(new User(1, "john@example.com"));
        users.add(new User(2, "alicia@example.com"));

        when(usersRepository.findAll())
                .thenReturn(users);

        assertThat(usersService.getUsers()).isEqualTo(users);
    }

    @Test
    void userServiceShouldInsertAUser() {
        User userToInsert = new User(1, "joe@example.com");
        when(usersRepository.save(any()))
                .thenReturn(userToInsert);

        User returnedUser = usersService.addUser(new User("joe@example.com", "some-pass"));

        assertThat(returnedUser.getEmail()).isEqualTo(userToInsert.getEmail());
        assertThat(returnedUser.getPassword()).isNull();
        assertThat(returnedUser.getId()).isNotNull();
    }

    @Test
    void userServiceShouldGetUserById() {
        User userToGet = new User(7, "alex@example.com");

        when(usersRepository.findUserById(anyLong()))
                .thenReturn(userToGet);

        assertThat(usersService.getUser(7))
                .isEqualTo(userToGet);
    }

    @Test
    void userServiceShouldUpdateUser() {
        User userThatWasUpdated = new User(8, "james@example.com");

        User userToUpdate = new User();
        userToUpdate.setEmail("james@example.com");
        User returnedUser = usersService.updateUser(userToUpdate);

        assertThat(returnedUser).isNotNull();
        assertThat(returnedUser.getId()).isNotNull();
        assertThat(returnedUser.getPassword()).isNull();
        assertThat(returnedUser.getEmail()).isEqualTo(userToUpdate.getEmail());
    }

    @Test
    void userServiceShouldDeleteUserById() {
        Map<String, Long> countReturn = new HashMap<String, Long>() {{
            put("count", (long) 32);
        }};

        when(usersRepository.count())
                .thenReturn((long) 32);

        assertThat(usersService.deleteUser(anyLong())).isEqualTo(countReturn);
    }

    @Test
    void userServiceShouldAuthenticate() {
        when(usersRepository.findUserByEmail(anyString()))
                .thenReturn(new User(9, "kevin@example.com", "random"));

        assertThat(usersService.authenticateUser(new User("kevin@example.com", "random")).isAuthenticated())
                .isEqualTo(true);

    }

}

