package com.galvanize.repository;

import com.galvanize.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {
    User findUserById(long anyLong);

    User findUserByEmail(String email);
}