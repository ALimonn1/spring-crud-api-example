package com.galvanize.service;

import com.galvanize.model.AuthResponse;
import com.galvanize.model.User;
import com.galvanize.repository.UsersRepository;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UsersService {
    private UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public List<User> getUsers() {
        List<User> usersToReturn = usersRepository.findAll();
        usersToReturn.stream().forEach(user -> user.setPassword(null));
        return usersToReturn;
    }

    public User addUser(User userRequest) {
        usersRepository.save(userRequest);
        userRequest.setPassword(null);
        return userRequest;
    }

    public User getUser(long id) {
        User userToReturn = usersRepository.findUserById(id);
        userToReturn.setPassword(null);
        return userToReturn;
    }

    public User updateUser(User userRequest) {
        usersRepository.save(userRequest);
        userRequest.setPassword(null);
        return userRequest;
    }

    public Map<String, Long> deleteUser(long id) {
        usersRepository.deleteById(id);
        return new HashMap<String, Long>() {{
            put("count", usersRepository.count());
        }};
    }

    public AuthResponse authenticateUser(User userRequest) {
        User userChecker = usersRepository.findUserByEmail(userRequest.getEmail());

        if (userChecker == null || userChecker.getPassword().isEmpty() ||
                !userChecker.getPassword().equals(userRequest.getPassword())) {
            userRequest.setPassword(null);
            return new AuthResponse(userRequest, false);
        }
        userRequest.setPassword(null);
        return new AuthResponse(userRequest, true);
    }
}