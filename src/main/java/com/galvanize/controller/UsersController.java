package com.galvanize.controller;

import com.galvanize.model.AuthResponse;
import com.galvanize.model.User;
import com.galvanize.model.UserRequest;
import com.galvanize.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class UsersController {

    private UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping
    @ResponseBody
    Map<String, List<User>> getUsers() {
        return new HashMap<String, List<User>>() {
            {
                put("users", usersService.getUsers());
            }
        };
    }

    @PostMapping
    @ResponseBody
    User addUser(@RequestBody UserRequest userRequest) {
        return usersService.addUser(userRequest.getUser());
    }

    @GetMapping("/{id}")
    @ResponseBody
    Map<String, User> getUser(@PathVariable long id) {
        return new HashMap<String, User>(){{
            put("user", usersService.getUser(id));
        }};
    }

    @PatchMapping("/{id}")
    @ResponseBody
    User updateUser(@PathVariable long id, @RequestBody User userRequest) {
        userRequest.setId(id);
        return usersService.updateUser(userRequest);
    }

    @DeleteMapping("/{id}")
    Map<String, Long> deleteUser(@PathVariable long id) {
        return usersService.deleteUser(id);
    }

    @PostMapping("/authenticate")
    AuthResponse authenticateUser(@RequestBody User user) {
        return usersService.authenticateUser(user);
    }
}
